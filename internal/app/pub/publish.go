package pub

import (
	"fmt"
	"os"
	"strings"
	"time"

	"gitlab.com/republico/library/go/extractor/pkg/file"
)

func Publish() error {
	now := fmt.Sprintf("%v", time.Now().UnixNano())	
	pathWork := Config.PathWork
	if pathWork != "" {
		if pathWork[len(pathWork) - 1] != '/' {
			pathWork += "/"
		}
	}
	pathWork += now + "/"

	pathFile := Config.PathFile
	if len(Config.PathFile) > 4 {
		if Config.PathFile[:4] == "http" {
			fileName := Config.PathFile[strings.LastIndex(Config.PathFile, "/") + 1:]
			fileOrigin := Config.PathFile
			pathFile = pathWork + fileName

			file.Download(fileOrigin, pathFile)
		}
	}

	if Config.TypeFile == "csv" {
		err := loaderCsv(pathFile, pathWork)
		if err != nil {
			return err
		}
	} else if Config.TypeFile == "json" {
		err := loaderCsv(pathFile, pathWork)
		if err != nil {
			return err
		}
	} else if Config.TypeFile == "xml" {
		err := loaderCsv(pathFile, pathWork)
		if err != nil {
			return err
		}
	}

	if Config.RemoveFileOrigin {
		err := os.RemoveAll(pathFile)
		if err != nil {
			return err
		}
	}

	return nil
}