package pub

type ConfigStruct struct {
	ServerNats string
	Name string
	Container string
	SeparatorSubject string
	PathFile string
	PathWork string
	TypeFile string
	SeparatorCsv string
	RemoveFileOrigin bool
}

var Config ConfigStruct