package pub

import (
	"path/filepath"
	"strings"

	"gitlab.com/republico/library/go/extractor/pkg/csv"
	"gitlab.com/republico/library/go/nats/pkg/publisher"
	"gitlab.com/republico/library/go/util/pkg/file"
)

var quote string

func loaderCsv(pathFile string, pathWork string) error {
	quote = "\""

	typeFile := strings.ToLower(file.GetExtension(pathFile))

	switch typeFile {
	case "zip":
		csvMap, err := csv.ReadZipFile(
			pathFile,
			pathWork,
			Config.SeparatorCsv,
		)
		if err != nil {
			return err
		}

		for filename, dataMap := range csvMap {
			publishData(filename, dataMap)
		}
	case "csv":
		dataMap, err := csv.ReadFile(
			pathFile,
			Config.SeparatorCsv,
		)
		if err != nil {
			return err
		}

		_, filename := filepath.Split(pathFile)
		filename = strings.Replace(filename, "." + typeFile, "", -1)
		publishData(filename, dataMap)
	}

	return nil
}

func publishData(filename string, dataMap [][]string) {
	header := quote + strings.Join(dataMap[0], quote + Config.SeparatorCsv + quote) + quote
	dataCsv := dataMap[1:]

	for _, dataArray := range dataCsv {
		blob := strings.Replace(filename, Config.Name + Config.SeparatorCsv, "", -1)
		subject := Config.Container + Config.SeparatorSubject + blob + Config.SeparatorSubject + Config.TypeFile + Config.SeparatorSubject + header
		data := quote + strings.Join(dataArray, quote + Config.SeparatorCsv + quote) + quote

		publisher.Execute(Config.ServerNats, subject, data)
	}
}