package main

import (
	"flag"
	"fmt"

	"gitlab.com/republico/library/go/util/pkg/exception"
	"gitlab.com/republico/service/nats_publisher/internal/app/pub"
)

func main() {
	server := flag.String("server", "localhost:4222", "Endereço(s) do(s) servidor(es) NATS.")
	name := flag.String("name", "republico", "Nome do processo. Usado para monitoração.")
	container := flag.String("container", "", "Nome do container do Azure.")
	separatorSubject := flag.String("separatorSubject", "|", "Separador usado para separar os componentes do subject.")
	pathFile := flag.String("pathfile", "", "Endereço do dado pra ser carregado.")
	pathWork := flag.String("pathwork", "", "Endereço utilizado para o tratamento do dado.")
	typeFile := flag.String("typefile", "", "Tipo do dado.")
	separatorCsv := flag.String("separatorCsv", ";", "Separador do CSV.")
	removeFileOrigin := flag.Bool("remove", false, "Remover arquivo de origem dos dados?")

	flag.Parse()

	pub.Config = pub.ConfigStruct{
		ServerNats: *server,
		Name: *name,
		Container: *container,
		SeparatorSubject: *separatorSubject,
		PathFile: *pathFile,
		PathWork: *pathWork,
		TypeFile: *typeFile,
		SeparatorCsv: *separatorCsv,
		RemoveFileOrigin: *removeFileOrigin,
	}

	fmt.Println("Starting sending messages")

	err := pub.Publish()
	exception.HandlerError(err)

	fmt.Println("Stopping sending messages")
}