# ETL Siconv

## Depuração

```
go run cmd/pub/main.go -server="localhost:4222" -name="siconv" -container="republico" -separatorSubject="|" -pathfile="C:/Teste/republico/siconv_consorcios.zip" -pathwork="C:/Teste/republico" -typefile="csv" -separatorCsv=";" -remove="false"
```

## Teste

### Testes Unitários

```
go test ./test -v -run TestLoadGeralOffline
go test ./test -v -run TestLoadConsorciosOffline
go test ./test -v -run TestLoadConsorciosOnline
```

### Testes do executável

```
pub -server="localhost:4222" -name="siconv" -container="republico" -separatorSubject="|" -pathfile="C:/Teste/republico/siconv_consorcios.zip" -pathwork="C:/Teste/republico" -typefile="csv" -separatorCsv=";" -remove="false"
```

## Build

```
GOOS=windows GOARCH=amd64 CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static" -s -w' -o bin/pub.exe cmd/pub/*.go
GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static" -s -w' -o bin/pub cmd/pub/*.go
```