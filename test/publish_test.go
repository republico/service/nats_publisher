package test

import (
	"testing"

	"gitlab.com/republico/service/nats_publisher/internal/app/pub"
)

func TestLoadGeralOffline(t *testing.T) {
	pub.Config = pub.ConfigStruct{
		ServerNats: "localhost:4222",
		Name: "siconv",
		Container: "raw",
		PathFile: "C:/Teste/republico/siconv.zip",
		PathWork: "C:/Teste/republico",
		TypeFile: "csv",
		Zip: true,
		SeparatorCsv: ";",
	}

	err := pub.Publish()
	if err != nil {
		t.Errorf("Ocorreu um erro.")
	}
}

func TestLoadConsorciosOffline(t *testing.T) {
	pub.Config = pub.ConfigStruct{
		ServerNats: "localhost:4222",
		Name: "siconv",
		Container: "raw",
		PathFile: "C:/Teste/republico/siconv_consorcios.zip",
		PathWork: "C:/Teste/republico",
		TypeFile: "csv",
		Zip: true,
		SeparatorCsv: ";",
	}

	err := pub.Publish()
	if err != nil {
		t.Errorf("Ocorreu um erro.")
	}
}

func TestLoadConsorciosOnline(t *testing.T) {
	pub.Config = pub.ConfigStruct{
		ServerNats: "localhost:4222",
		Name: "siconv",
		Container: "raw",
		PathFile: "http://portal.convenios.gov.br/images/docs/CGSIS/csv/siconv_consorcios.csv.zip",
		PathWork: "C:/Teste/republico",
		TypeFile: "csv",
		Zip: true,
		SeparatorCsv: ";",
	}

	err := pub.Publish()
	if err != nil {
		t.Errorf("Ocorreu um erro.")
	}
}